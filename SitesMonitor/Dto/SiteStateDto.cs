﻿using System;

namespace SitesMonitor.Dto
{
    public class SiteStateDto
    {
        public SiteStateDto(string url)
        {
            Url = url;
            Update = DateTime.Now;
        }

        public string Url { get; set; }

        public bool? Available { get; set; }

        public DateTime Update { get; set; }
    }
}