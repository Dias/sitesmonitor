﻿import * as React from 'react';
import { connect } from 'react-redux';
import * as SitesState from '../store/Sites';
import { ApplicationState } from '../store';
import SiteState = SitesState.SiteState;

class Home extends React.Component<any, any> {
    componentWillMount() {
        this.props.requestSitesStates();
    }

    formatDate(d: Date) {
        let date = new Date(d);
        let ms = (num: number) =>
            num > 9
                ? num.toString()
                : '0' + num.toString();
        return `${ms(date.getHours())}:${ms(date.getMinutes())}:${ms(date.getSeconds())}`
    }

    private renderSites() {
        return <table className='table'>
                   <thead>
                   <tr>
                        <th className="align-center-v">Ссылка</th>
                       <th className="align-center">Обновлено</th>
                       <th className="align-center">Доступность</th>
                   </tr>
                   </thead>
                   <tbody>
                    {this.props.states && this.props.states.map((state: SiteState) =>
                        <tr key={state.url}>
                            <td className="align-center-v">{state.url}</td>
                            <td className="align-center">{this.formatDate(state.update)}</td>
                            <td className="align-center">{state.available
                                ? <img className="avail" title="Сайт доступен" src="/images/available.png" />
                                : state.available === false
                                    ? <img className="avail" title="Сайт не доступен" src="/images/not-available.png" />
                                    : <img className="avail" title="Состояние не определено" src="/images/not-defined.png" />}</td>
                    </tr>
                   )}
                   </tbody>
               </table>;
    }

    public render() {
        return <div>
            <h1>Доступность сайтов</h1>
            { this.renderSites() }
        </div>;
    }
}

export default connect(
    (state: ApplicationState) => state.sites, 
    SitesState.actionCreators                 
)(Home) as typeof Home;