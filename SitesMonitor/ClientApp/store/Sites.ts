﻿import { Action, Reducer } from 'redux';
import { fetch, addTask } from 'domain-task';
import { AppThunkAction } from './';

export interface SiteState {
    url: string;
    available: boolean;
    update: Date;
}

export interface SitesState {
    states: SiteState[];
}

interface ReceiveSitesStatesAction {
    type: 'RECEIVE_SITES_STATES';
    states: SiteState[];
}

type KnownAction = ReceiveSitesStatesAction;

export const actionCreators = {
    requestSitesStates: (): AppThunkAction<KnownAction> => (dispatch, getState) => {
        let fetchTask = fetch(`api/sites`)
            .then(response => response.json() as Promise<SiteState[]>)
            .then(data => {
                dispatch({ type: 'RECEIVE_SITES_STATES', states: data });
            });
        addTask(fetchTask);
    }
};

const unloadedState: SitesState = { states: [] };

export const reducer: Reducer<SitesState> = (state: SitesState, incomingAction: Action) => {
    const action = incomingAction as KnownAction;
    switch (action.type) {
        case 'RECEIVE_SITES_STATES':
            return {
                states: action.states
            };
    }
    return state || unloadedState;
};