﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using SitesMonitor.Domain;

namespace SitesMonitor
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<SitesMonitorContext>
    {
        public SitesMonitorContext CreateDbContext(string[] args)
        {
            var envName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            Console.WriteLine($"Environment - {envName}");
            
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{envName}.json", optional: true)
                .Build();

            var builder = new DbContextOptionsBuilder<SitesMonitorContext>();

            var connectionString = configuration.GetConnectionString("DefaultConnection");

            builder.UseSqlServer(connectionString);

            return new SitesMonitorContext(builder.Options);
        }
    }
}