﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using SitesMonitor.Domain;
using SitesMonitor.Dto;
using SitesMonitor.Schedulers;

namespace SitesMonitor.Controllers
{
    [Produces("application/json")]
    [Route("api/sites")]
    public class SitesController : Controller
    {
        private readonly SitesMonitorContext _context;
        private readonly ICheckerScheduler _checkService;

        public SitesController(SitesMonitorContext context, ICheckerScheduler checkService)
        {
            _context = context;
            _checkService = checkService;
        }

        [HttpGet]
        public IEnumerable<SiteStateDto> Get()
        {
            _checkService.StartAsync();
            return _context.Sites.Select(x => new SiteStateDto(x.Url));
        }
    }
}