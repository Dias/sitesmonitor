using Microsoft.AspNetCore.Mvc;

namespace SitesMonitor.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}