﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SitesMonitor.Dto;

namespace SitesMonitor.Services
{
    public interface IClientsNotifier
    {
        Task Notify(IEnumerable<SiteStateDto> stateDtos);
    }
}