﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using SitesMonitor.Dto;

namespace SitesMonitor.Services
{
    public class ClientsNotifier : IClientsNotifier
    {
        private readonly IHubContext<SitesHub> _sitesHub;

        public ClientsNotifier(IHubContext<SitesHub> sitesHub)
        {
            _sitesHub = sitesHub;
        }

        public async Task Notify(IEnumerable<SiteStateDto> stateDtos)
        {
            await _sitesHub.Clients.All.InvokeAsync("states", stateDtos);
        }
    }
}