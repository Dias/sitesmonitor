﻿using System.Net;

namespace SitesMonitor.Services
{
    public class AvailabilityChecker: IAvailabilityChecker
    {
        public bool SiteIsAvailable(string url)
        {
            try
            {
                var request = WebRequest.Create(url);
                request.Method = "HEAD";
                var response = (HttpWebResponse)request.GetResponse();
                var result = response.StatusCode == HttpStatusCode.OK;
                response.Close();
                return result;
            }
            catch (WebException)
            {
                return false;
            }
        }
    }
}