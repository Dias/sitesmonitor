﻿namespace SitesMonitor.Services
{
    public interface IAvailabilityChecker
    {
        bool SiteIsAvailable(string url);
    }
}