﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using SitesMonitor.Dto;

namespace SitesMonitor.Services
{
    public class SitesHub : Hub
    {
        public async Task SendStates(string message)
        {
            await this.Clients.All.InvokeAsync("SendStates", message);
        }

        public async void InvokeMessages(List<SiteStateDto> stateDtos)
        {
            await Clients.All.InvokeAsync("states", stateDtos);
        }
    }
}