using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SitesMonitor.Domain;
using SitesMonitor.Domain.Models;
using SitesMonitor.Domain.Seed;
using SitesMonitor.Schedulers;
using SitesMonitor.Services;

namespace SitesMonitor
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<SitesMonitorContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddScoped<DbContext, SitesMonitorContext>();
            services.AddMvc()
                .AddJsonOptions(
                    options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                );
            services.AddSignalR();
            services.AddScoped<IClientsNotifier, ClientsNotifier>();
            services.AddScoped<IAvailabilityChecker, AvailabilityChecker>();
            services.AddSingleton<ICheckerScheduler, CheckerScheduler>(provider =>
            {
                var serviceScope = provider.GetService<IServiceScopeFactory>().CreateScope();
                var broadcastMessages = serviceScope.ServiceProvider.GetService<IClientsNotifier>();
                var availabilityChecker = serviceScope.ServiceProvider.GetService<IAvailabilityChecker>();
                var intervals = Enumerable.Empty<Interval>();
                var sites = Enumerable.Empty<Site>();
                using (var context = serviceScope.ServiceProvider.GetService<SitesMonitorContext>())
                {
                    intervals = context.Intervals.ToList();
                    sites = context.Sites.ToList();
                }
                return new CheckerScheduler(intervals, sites, broadcastMessages, availabilityChecker);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true,
                    ReactHotModuleReplacement = true
                });
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            #region Seed

            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<SitesMonitorContext>();
                if (context.Database.GetAppliedMigrations().Any())
                {
                    context.EnsureSeeded();
                }
            }

            #endregion

            app.UseStaticFiles();

            app.UseSignalR(routes =>
            {
                routes.MapHub<SitesHub>("states");
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");

                routes.MapSpaFallbackRoute(
                    name: "spa-fallback",
                    defaults: new { controller = "Home", action = "Index" });
            });
        }
    }
}
