﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SitesMonitor.Domain.Models;
using SitesMonitor.Dto;
using SitesMonitor.Services;

namespace SitesMonitor.Schedulers
{
    public class CheckerScheduler : ICheckerScheduler
    {
        private readonly CancellationToken _cancellationToken;
        private readonly IEnumerable<Interval> _intervals;
        private readonly IEnumerable<Site> _sites;
        private readonly IClientsNotifier _clientsNotifier;
        private readonly IAvailabilityChecker _checker;

        public CheckerScheduler(IEnumerable<Interval> intervals, IEnumerable<Site> sites, IClientsNotifier clientsNotifier, IAvailabilityChecker checker)
        {
            var cancellationTokenSource = new CancellationTokenSource();
            _cancellationToken = cancellationTokenSource.Token;

            _intervals = intervals;
            _sites = sites;
            _clientsNotifier = clientsNotifier;
            _checker = checker;
        }

        private Task _executingTask;

        public Task ExecutingTask => _executingTask;

        public Task StartAsync()
        {
            if (_executingTask != null)
            {
                return Task.CompletedTask;
            }

            // Store the task we're executing
            _executingTask = ExecuteAsync(_cancellationToken);
            return _executingTask;
        }

        private async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            var intervalsQueue = new Queue<Interval>(_intervals);
            while (!cancellationToken.IsCancellationRequested)
            {
                if (!intervalsQueue.Any())
                {
                    break;
                }

                var interval = intervalsQueue.Dequeue();
                await Task.Delay(TimeSpan.FromSeconds(interval.Value), cancellationToken);
                var availStates = _sites.Select(site => new SiteStateDto(site.Url)
                {
                    Available = _checker.SiteIsAvailable(site.Url)
                }).ToList();
                await _clientsNotifier.Notify(availStates);
            }
        }
    }
}