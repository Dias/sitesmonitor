﻿using System.Threading.Tasks;

namespace SitesMonitor.Schedulers
{
    public interface ICheckerScheduler
    {
        Task StartAsync();
    }
}