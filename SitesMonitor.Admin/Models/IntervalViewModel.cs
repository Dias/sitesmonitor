﻿namespace SitesMonitor.Admin.Models
{
    public class IntervalViewModel
    {
        public IntervalViewModel()
        {
        }

        public IntervalViewModel(int id, int value)
        {
            Id = id;
            Value = value;
        }

        public int Id { get; set; }

        public int Value { get; set; }
    }
}