﻿namespace SitesMonitor.Admin.Models
{
    public class SiteViewModel
    {
        public SiteViewModel()
        {
        }

        public SiteViewModel(int id, string url)
        {
            Id = id;
            Url = url;
        }

        public int Id { get; set; }

        public string Url { get; set; }
    }
}