﻿function deleteSite(id) {
    if (!confirm("Вы действительно хотите удалить сайт?")) {
        return;
    }
    $.post("/sites/delete", { id }, function() {
        location.reload();
    });
}

function deleteInterval(id) {
    if (!confirm("Вы действительно хотите удалить интервал?")) {
        return;
    }
    $.post("/intervals/delete", { id }, function () {
        location.reload();
    });
}