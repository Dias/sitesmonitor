﻿using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SitesMonitor.Admin.Models;
using SitesMonitor.Domain;
using SitesMonitor.Domain.Models;

namespace SitesMonitor.Admin.Controllers
{
    [Authorize]
    public class IntervalsController : Controller
    {
        private readonly SitesMonitorContext _context;

        public IntervalsController(SitesMonitorContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var intervals = _context.Intervals.Select(x => new IntervalViewModel(x.Id, x.Value));
            return View(intervals);
        }

        [HttpPost]
        public IActionResult Delete(IntervalViewModel intervalVM)
        {
            var interval = _context.Intervals.Find(intervalVM.Id);
            if (interval != null)
            {
                _context.Intervals.Remove(interval);
                _context.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Add(IntervalViewModel intervalVM)
        {
            var interval = new Interval
            {
                Value = intervalVM.Value
            };
            _context.Intervals.Add(interval);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}