﻿using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SitesMonitor.Admin.Models;
using SitesMonitor.Domain;
using SitesMonitor.Domain.Models;

namespace SitesMonitor.Admin.Controllers
{
    [Authorize]
    public class SitesController : Controller
    {
        private readonly SitesMonitorContext _context;

        public SitesController(SitesMonitorContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var sites = _context.Sites.Select(x => new SiteViewModel(x.Id, x.Url));
            return View(sites);
        }

        [HttpPost]
        public IActionResult Delete(SiteViewModel siteVM)
        {
            var site = _context.Sites.Find(siteVM.Id);
            if (site != null)
            {
                _context.Sites.Remove(site);
                _context.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult Add(SiteViewModel siteVM)
        {
            var site = new Site
            {
                Url = siteVM.Url
            };
            _context.Sites.Add(site);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}