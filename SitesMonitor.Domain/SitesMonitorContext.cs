﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SitesMonitor.Domain.Models;

namespace SitesMonitor.Domain
{
    public class SitesMonitorContext : IdentityDbContext<IdentityUser<int>, IdentityRole<int>, int>
    {
        public SitesMonitorContext(DbContextOptions<SitesMonitorContext> options) : base(options)
        {
            
        }

        protected SitesMonitorContext() { }

        public virtual DbSet<Site> Sites { get; set; }

        public virtual DbSet<Interval> Intervals { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}