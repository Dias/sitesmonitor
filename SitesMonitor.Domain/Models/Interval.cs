﻿namespace SitesMonitor.Domain.Models
{
    public class Interval
    {
        public Interval()
        {
        }

        public Interval(int value)
        {
            Value = value;
        }

        public int Id { get; set; }

        /// <summary>
        /// Интервал времени в секундах
        /// </summary>
        public int Value { get; set; }
    }
}