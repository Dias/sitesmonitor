﻿namespace SitesMonitor.Domain.Models
{
    public class Site
    {
        public Site()
        {
        }

        public Site(string url)
        {
            Url = url;
        }

        public int Id { get; set; }

        public string Url { get; set; }
    }
}