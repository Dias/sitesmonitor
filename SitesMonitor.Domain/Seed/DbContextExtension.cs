﻿using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using SitesMonitor.Domain.Models;

namespace SitesMonitor.Domain.Seed
{
    public static class DbContextExtension
    {
        public static bool AllMigrationsApplied(this DbContext context)
        {
            var applied = context.GetService<IHistoryRepository>()
                .GetAppliedMigrations()
                .Select(m => m.MigrationId);

            var total = context.GetService<IMigrationsAssembly>()
                .Migrations
                .Select(m => m.Key);

            return !total.Except(applied).Any();
        }

        public static void EnsureSeeded(this SitesMonitorContext context)
        {
            context.SeedSites();
            context.SeedIntervals();
            context.SeedUsers();

            context.SaveChanges();
        }

        private static void SeedSites(this SitesMonitorContext context)
        {
            if (context.Sites.Any())
            {
                return;
            }
            var urls = new[]
            {
                "http://ya.ru",
                "http://google.com",
                "http://yahoo.com"
            };
            foreach (var url in urls)
            {
                if (context.Sites.Any(x => x.Url == url))
                {
                    continue;
                }
                var site = new Site(url);
                context.Sites.Add(site);
            }
        }

        private static void SeedIntervals(this SitesMonitorContext context)
        {
            if (context.Intervals.Any())
            {
                return;
            }
            var values = new[]
            {
                15,
                5,
                10,
                15
            };
            foreach (var val in values)
            {
                if (context.Intervals.Any(x => x.Value == val))
                {
                    continue;
                }
                var interval = new Interval(val);
                context.Intervals.Add(interval);
            }
        }

        private static void SeedUsers(this SitesMonitorContext context)
        {
            if (context.Users.Any(x => x.Email == "admin@admin.com"))
            {
                return;
            }
            var user = new IdentityUser<int>("admin@admin.com")
            {
                Email = "admin@admin.com",
                EmailConfirmed = true
            };
            context.Users.Add(user);
        }
    }
}