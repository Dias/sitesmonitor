﻿using System.Threading.Tasks;
using SitesMonitor.Schedulers;

namespace SitesMonitor.Tests
{
    public class CheckerSchedulerMock: ICheckerScheduler
    {
        public bool Started { get; private set; }
        public Task StartAsync()
        {
            Started = true;
            return Task.CompletedTask;
        }
    }
}