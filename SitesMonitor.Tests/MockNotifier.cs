﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SitesMonitor.Dto;
using SitesMonitor.Services;

namespace SitesMonitor.Tests
{
    public class MockNotifier : IClientsNotifier
    {
        public IEnumerable<SiteStateDto> StateDtos { get; private set; }

        public Task Notify(IEnumerable<SiteStateDto> stateDtos)
        {
            StateDtos = stateDtos;
            return Task.CompletedTask;
        }
    }
}