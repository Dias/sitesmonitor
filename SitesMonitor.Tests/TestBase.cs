﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Moq;
using SitesMonitor.Domain;
using SitesMonitor.Domain.Models;

namespace SitesMonitor.Tests
{
    public abstract class TestBase
    {
        protected IQueryable<Interval> Intervals = new List<Interval>
        {
            new Interval(3),
            new Interval(5)
        }.AsQueryable();

        protected IQueryable<Site> Sites = new List<Site>
        {
            new Site("http://ya.ru"),
            new Site("http://google.com")
        }.AsQueryable();

        protected SitesMonitorContext Context;

        protected TestBase()
        {
            var mockSet = new Mock<DbSet<Site>>();
            mockSet.As<IQueryable<Site>>().Setup(m => m.Provider).Returns(Sites.Provider);
            mockSet.As<IQueryable<Site>>().Setup(m => m.Expression).Returns(Sites.Expression);
            mockSet.As<IQueryable<Site>>().Setup(m => m.ElementType).Returns(Sites.ElementType);
            mockSet.As<IQueryable<Site>>().Setup(m => m.GetEnumerator()).Returns(Sites.GetEnumerator());

            var mockContext = new Mock<SitesMonitorContext>();
            mockContext.Setup(c => c.Sites).Returns(mockSet.Object);
            Context = mockContext.Object;
        }
    }
}