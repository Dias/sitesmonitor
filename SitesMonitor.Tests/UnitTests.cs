﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using SitesMonitor.Controllers;
using SitesMonitor.Domain.Models;
using SitesMonitor.Schedulers;
using SitesMonitor.Services;
using Xunit;

namespace SitesMonitor.Tests
{
    public class UnitTests: TestBase
    {
        [Fact]
        public async void TestSchedulerBroadcast()
        {
            // Arrange
            var mockBroadcast = new MockNotifier();

            var mockChecker = new Mock<IAvailabilityChecker>();
            mockChecker
                .Setup(x => x.SiteIsAvailable(It.IsAny<string>()))
                .Returns(true);

            // Act
            var scheduler = new CheckerScheduler(Intervals, Sites, mockBroadcast, mockChecker.Object);
            await scheduler.StartAsync();

            // Assert
            Assert.NotNull(mockBroadcast.StateDtos);
            Assert.Equal(Sites.Count(), mockBroadcast.StateDtos.Count());
            Assert.Equal(Sites.ElementAt(1).Url, mockBroadcast.StateDtos.ElementAt(1).Url);
            Assert.True(mockBroadcast.StateDtos.ElementAt(1).Available);
        }

        [Fact]
        public async void TestSchedulerIntervals()
        {
            // Arrange
            var mockBroadcast = new MockNotifier();

            var mockChecker = new Mock<IAvailabilityChecker>();
            mockChecker
                .Setup(x => x.SiteIsAvailable(It.IsAny<string>()))
                .Returns(true);

            var intervals = new List<Interval>
            {
                new Interval(1),
                new Interval(0),
                new Interval(1),
                new Interval(1)
            }.AsQueryable();

            // Act
            var scheduler = new CheckerScheduler(intervals, Sites, mockBroadcast, mockChecker.Object);
            var startTime = DateTime.Now;
            await scheduler.StartAsync();
            var endTime = DateTime.Now;
            var elapsed = endTime - startTime;
            
            // Assert
            Assert.Equal(intervals.Sum(x => x.Value), elapsed.Seconds);
        }

        [Fact]
        public void TestSitesController()
        {
            // Arrange
            var mockScheduler = new CheckerSchedulerMock();

            // Act
            var controller = new SitesController(Context, mockScheduler);
            var siteDtos = controller.Get();

            // Assert
            Assert.Equal(2, siteDtos.Count());
            Assert.Null(siteDtos.ElementAt(0).Available);
            Assert.True(mockScheduler.Started);
        }
    }
}