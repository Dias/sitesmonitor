﻿using System.Linq;
using Moq;
using SitesMonitor.Controllers;
using SitesMonitor.Schedulers;
using SitesMonitor.Services;
using Xunit;

namespace SitesMonitor.Tests
{
    public class IntegrationTests: TestBase
    {
        [Fact]
        public async void TestIntegration()
        {
            // Arrange
            var mockBroadcast = new MockNotifier();

            var mockChecker = new Mock<IAvailabilityChecker>();
            mockChecker
                .Setup(x => x.SiteIsAvailable(It.IsAny<string>()))
                .Returns(true);

            // Act
            var scheduler = new CheckerScheduler(Intervals, Sites, mockBroadcast, mockChecker.Object);
            var controller = new SitesController(Context, scheduler);
            controller.Get();
            await scheduler.ExecutingTask;

            // Assert
            Assert.NotNull(mockBroadcast.StateDtos);
            Assert.Equal(Sites.Count(), mockBroadcast.StateDtos.Count());
            Assert.Equal(Sites.ElementAt(1).Url, mockBroadcast.StateDtos.ElementAt(1).Url);
            Assert.True(mockBroadcast.StateDtos.ElementAt(1).Available);
        }
    }
}